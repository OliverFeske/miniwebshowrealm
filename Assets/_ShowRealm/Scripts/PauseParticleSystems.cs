﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseParticleSystems : MonoBehaviour
{
	[SerializeField] private Transform effectsParent;
	[SerializeField] private ParticleSystem[] pSystems;
	private bool isPaused;

	void Start()
	{
		pSystems = effectsParent.GetComponentsInChildren<ParticleSystem>();
	}

	void Update()
    {
		if (Input.GetButtonDown("Jump"))
		{
			if (!isPaused)
			{
				for (int i = 0; i < pSystems.Length; i++)
				{
					pSystems[i].Pause();
				}
				isPaused = true;
			}
			else
			{
				for (int i = 0; i < pSystems.Length; i++)
				{
					pSystems[i].Play();
				}
				isPaused = false;
			}
		}
    }
}
