// GENERATED AUTOMATICALLY FROM 'Assets/_ShowRealm/InputActionMaps/CameraControls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class CameraControls : IInputActionCollection, IDisposable
{
    private InputActionAsset asset;
    public CameraControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""CameraControls"",
    ""maps"": [
        {
            ""name"": ""LockedCameraMovement"",
            ""id"": ""4913ce17-3d88-4cfa-bb61-25a19a5a76c7"",
            ""actions"": [
                {
                    ""name"": ""Movement"",
                    ""type"": ""Button"",
                    ""id"": ""8c6e764e-266f-467a-a708-ab1d423034a8"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Scroll"",
                    ""type"": ""Button"",
                    ""id"": ""cc7956a5-f39e-4bd3-b6c8-cd3bba9b098b"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""WASD"",
                    ""id"": ""d56d9848-bb73-45b9-856a-52ed3227e253"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""2fa482b4-f0de-4bf6-8f46-adc120f3481b"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""09036c61-036a-49f4-a6d5-841d77350b9b"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""202f3309-51a1-4a0d-980f-e58de4c6e7f5"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""0f8cc85c-62b9-4d28-a268-a85600f71e72"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""bff17f9e-c73b-4afb-b04a-49478d54457f"",
                    ""path"": ""<Mouse>/scroll/y"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Scroll"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""FreeCameraMovement"",
            ""id"": ""8e2ff37a-8225-4b1b-b6c4-b9f932ad250e"",
            ""actions"": [
                {
                    ""name"": ""XZMovement"",
                    ""type"": ""Button"",
                    ""id"": ""437e0e01-c952-4e55-83ce-36a5bd94ab7c"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MouseButtonPressed"",
                    ""type"": ""Button"",
                    ""id"": ""e0dd1e28-c9ea-4025-b96f-ee5432c0b2ff"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""XZ"",
                    ""id"": ""27ad3868-e78c-4f2a-8dea-845bec9477e0"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""XZMovement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""a616d4a5-861e-4c55-9790-38827d99aa04"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""XZMovement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""02bf574f-2770-4454-997b-734171d1b638"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""XZMovement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""2332e308-3987-4b85-801c-919fb0e85dfd"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""XZMovement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""1cc16622-fc73-4e91-9638-84b7cb152762"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""XZMovement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Y"",
                    ""id"": ""619d1738-5cbd-4ecb-a051-b72f1680ddd0"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""bed91b59-ab90-4b11-bbff-c3cb6915ca5c"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""514037b5-c317-4b38-b83c-99f4d26b24d6"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""b39b0887-c759-43a0-888a-65c11344c563"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MouseButtonPressed"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // LockedCameraMovement
        m_LockedCameraMovement = asset.FindActionMap("LockedCameraMovement", throwIfNotFound: true);
        m_LockedCameraMovement_Movement = m_LockedCameraMovement.FindAction("Movement", throwIfNotFound: true);
        m_LockedCameraMovement_Scroll = m_LockedCameraMovement.FindAction("Scroll", throwIfNotFound: true);
        // FreeCameraMovement
        m_FreeCameraMovement = asset.FindActionMap("FreeCameraMovement", throwIfNotFound: true);
        m_FreeCameraMovement_XZMovement = m_FreeCameraMovement.FindAction("XZMovement", throwIfNotFound: true);
        m_FreeCameraMovement_MouseButtonPressed = m_FreeCameraMovement.FindAction("MouseButtonPressed", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // LockedCameraMovement
    private readonly InputActionMap m_LockedCameraMovement;
    private ILockedCameraMovementActions m_LockedCameraMovementActionsCallbackInterface;
    private readonly InputAction m_LockedCameraMovement_Movement;
    private readonly InputAction m_LockedCameraMovement_Scroll;
    public struct LockedCameraMovementActions
    {
        private CameraControls m_Wrapper;
        public LockedCameraMovementActions(CameraControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Movement => m_Wrapper.m_LockedCameraMovement_Movement;
        public InputAction @Scroll => m_Wrapper.m_LockedCameraMovement_Scroll;
        public InputActionMap Get() { return m_Wrapper.m_LockedCameraMovement; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(LockedCameraMovementActions set) { return set.Get(); }
        public void SetCallbacks(ILockedCameraMovementActions instance)
        {
            if (m_Wrapper.m_LockedCameraMovementActionsCallbackInterface != null)
            {
                Movement.started -= m_Wrapper.m_LockedCameraMovementActionsCallbackInterface.OnMovement;
                Movement.performed -= m_Wrapper.m_LockedCameraMovementActionsCallbackInterface.OnMovement;
                Movement.canceled -= m_Wrapper.m_LockedCameraMovementActionsCallbackInterface.OnMovement;
                Scroll.started -= m_Wrapper.m_LockedCameraMovementActionsCallbackInterface.OnScroll;
                Scroll.performed -= m_Wrapper.m_LockedCameraMovementActionsCallbackInterface.OnScroll;
                Scroll.canceled -= m_Wrapper.m_LockedCameraMovementActionsCallbackInterface.OnScroll;
            }
            m_Wrapper.m_LockedCameraMovementActionsCallbackInterface = instance;
            if (instance != null)
            {
                Movement.started += instance.OnMovement;
                Movement.performed += instance.OnMovement;
                Movement.canceled += instance.OnMovement;
                Scroll.started += instance.OnScroll;
                Scroll.performed += instance.OnScroll;
                Scroll.canceled += instance.OnScroll;
            }
        }
    }
    public LockedCameraMovementActions @LockedCameraMovement => new LockedCameraMovementActions(this);

    // FreeCameraMovement
    private readonly InputActionMap m_FreeCameraMovement;
    private IFreeCameraMovementActions m_FreeCameraMovementActionsCallbackInterface;
    private readonly InputAction m_FreeCameraMovement_XZMovement;
    private readonly InputAction m_FreeCameraMovement_MouseButtonPressed;
    public struct FreeCameraMovementActions
    {
        private CameraControls m_Wrapper;
        public FreeCameraMovementActions(CameraControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @XZMovement => m_Wrapper.m_FreeCameraMovement_XZMovement;
        public InputAction @MouseButtonPressed => m_Wrapper.m_FreeCameraMovement_MouseButtonPressed;
        public InputActionMap Get() { return m_Wrapper.m_FreeCameraMovement; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(FreeCameraMovementActions set) { return set.Get(); }
        public void SetCallbacks(IFreeCameraMovementActions instance)
        {
            if (m_Wrapper.m_FreeCameraMovementActionsCallbackInterface != null)
            {
                XZMovement.started -= m_Wrapper.m_FreeCameraMovementActionsCallbackInterface.OnXZMovement;
                XZMovement.performed -= m_Wrapper.m_FreeCameraMovementActionsCallbackInterface.OnXZMovement;
                XZMovement.canceled -= m_Wrapper.m_FreeCameraMovementActionsCallbackInterface.OnXZMovement;
                MouseButtonPressed.started -= m_Wrapper.m_FreeCameraMovementActionsCallbackInterface.OnMouseButtonPressed;
                MouseButtonPressed.performed -= m_Wrapper.m_FreeCameraMovementActionsCallbackInterface.OnMouseButtonPressed;
                MouseButtonPressed.canceled -= m_Wrapper.m_FreeCameraMovementActionsCallbackInterface.OnMouseButtonPressed;
            }
            m_Wrapper.m_FreeCameraMovementActionsCallbackInterface = instance;
            if (instance != null)
            {
                XZMovement.started += instance.OnXZMovement;
                XZMovement.performed += instance.OnXZMovement;
                XZMovement.canceled += instance.OnXZMovement;
                MouseButtonPressed.started += instance.OnMouseButtonPressed;
                MouseButtonPressed.performed += instance.OnMouseButtonPressed;
                MouseButtonPressed.canceled += instance.OnMouseButtonPressed;
            }
        }
    }
    public FreeCameraMovementActions @FreeCameraMovement => new FreeCameraMovementActions(this);
    public interface ILockedCameraMovementActions
    {
        void OnMovement(InputAction.CallbackContext context);
        void OnScroll(InputAction.CallbackContext context);
    }
    public interface IFreeCameraMovementActions
    {
        void OnXZMovement(InputAction.CallbackContext context);
        void OnMouseButtonPressed(InputAction.CallbackContext context);
    }
}
