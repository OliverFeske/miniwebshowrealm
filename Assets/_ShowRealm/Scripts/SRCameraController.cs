﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using static CameraControls;

namespace ShowRealm
{
	public class SRCameraController : MonoBehaviour/*, ILockedCameraMovementActions, IFreeCameraMovementActions*/
	{
		[Header("Visible In Build")]
		public CameraMovementType cameraMovementType = CameraMovementType.FreeMove;
		public Vector3 targetOffset;
		public float minDistance = 1f;
		public float maxDistance = 1000f;
		public float scrollSpeed = 2f;
		public float xSpeed = 100f;
		public float ySpeed = 100f;
		public float zSpeed = 100f;
		[Header("Pre Build Settings")]
		[SerializeField] private Transform defaultTarget;
		[SerializeField] private float defaultDistance = 5f;

		//private CameraControls cameraControls;
		private Transform target;
		private Vector2 initialMousePos;
		private float distance;
		private float lockedX;
		private float lockedY;
		private float freeX;
		private float freeY;
		private float freeZ;
		private float freeRightMouseButtonWasPressedBefore;

		#region Input Properties
		// locked Movement
		private Vector2 lockedV2 { get; set; }
		private float scrollAmount { get; set; }
		// free movement
		//private Vector2 freeXZmove { get; set; }
		//private float freeYmove { get; set; }
		//private float freeRightMouseButtonPressed { get; set; }
		private Vector2 freeXZmove;
		private float freeYmove;
		private bool freeRightMouseButtonPressed;
		#endregion

		#region MonoBehaviour
		void Awake()
		{
			//cameraControls = new CameraControls();
			//cameraControls.LockedCameraMovement.SetCallbacks(null);
			//cameraControls.FreeCameraMovement.SetCallbacks(this);
		}
		void Start()
		{
			Cursor.lockState = CursorLockMode.None;
			target = defaultTarget;
			distance = defaultDistance;
			lockedX = transform.eulerAngles.y;
			lockedY = transform.eulerAngles.x;
		}
		void Update()
		{
			freeXZmove.x = Input.GetAxis("Horizontal");
			freeXZmove.y = Input.GetAxis("Vertical");
			freeYmove = Input.GetAxis("YMovement");

			if (Input.GetMouseButtonDown(0))
			{
				freeRightMouseButtonPressed = true;
				initialMousePos = Mouse.current.position.ReadValue();
			}
			else if(Input.GetMouseButtonUp(0))
			{
				freeRightMouseButtonPressed = false;
			}
		}
		void LateUpdate()
		{
			switch (cameraMovementType)
			{
				case CameraMovementType.LockedTarget:
					lockedX -= lockedV2.x * xSpeed * 0.01f;
					lockedY += lockedV2.y * ySpeed * 0.01f;

					distance -= scrollAmount * scrollSpeed * 0.001f;

					if (distance < minDistance)
						distance = minDistance;
					if (distance > maxDistance)
						distance = maxDistance;

					Quaternion rotation = Quaternion.Euler(lockedY, lockedX, 0f);
					Vector3 position = rotation * new Vector3(0f, 0f, -distance) + target.position + targetOffset;

					transform.rotation = rotation;
					transform.position = position;
					break;
				case CameraMovementType.FreeMove:
					transform.localPosition += (transform.right * freeXZmove.x * xSpeed + transform.up * freeYmove * ySpeed + transform.forward * freeXZmove.y * zSpeed) * 0.001f;

					if (freeRightMouseButtonPressed)
					{
						Vector2 newMousePos = Mouse.current.position.ReadValue();
						Vector2 mousePosDelta = initialMousePos - newMousePos;

						freeX -= mousePosDelta.x * xSpeed * 0.001f;
						freeY += mousePosDelta.y * ySpeed * 0.001f;
						freeY = Mathf.Clamp(freeY, -90f, 90f);

						Quaternion rot = Quaternion.Euler(freeY, freeX, 0f);
						transform.rotation = rot;

						initialMousePos = newMousePos;
					}
					break;
				default:
					break;
			}
		}
		//void OnEnable()
		//{
		//	cameraControls.Enable();
		//}
		//void OnDisable()
		//{
		//	cameraControls.Disable();
		//}
		#endregion

		#region public methods
		//public void ChangeTarget(Transform newTarget)
		//{
		//	target = newTarget;
		//}
		//public void ToggleMovementType()
		//{
		//	if (cameraMovementType == CameraMovementType.FreeMove)
		//	{
		//		cameraMovementType = CameraMovementType.LockedTarget;
		//		cameraControls.LockedCameraMovement.SetCallbacks(this);
		//		cameraControls.FreeCameraMovement.SetCallbacks(null);
		//	}
		//	else
		//	{
		//		cameraMovementType = CameraMovementType.FreeMove;
		//		cameraControls.FreeCameraMovement.SetCallbacks(this);
		//		cameraControls.LockedCameraMovement.SetCallbacks(null);
		//	}
		//}
		#endregion

		#region Input Methods
		//public void OnMovement(InputAction.CallbackContext context)
		//{
		//	var inputV2 = context.ReadValue<Vector2>();
		//	lockedV2 = inputV2;
		//}
		//public void OnScroll(InputAction.CallbackContext context)
		//{
		//	var scroll = context.ReadValue<float>();
		//	scrollAmount = scroll;
		//}
		//public void OnXZMovement(InputAction.CallbackContext context)
		//{
		//	var moveXZ = context.ReadValue<Vector2>();
		//	freeXZmove = moveXZ;
		//}
		//public void OnYMovement(InputAction.CallbackContext context)
		//{
		//	var moveY = context.ReadValue<float>();
		//	freeYmove = moveY;
		//}
		//public void OnMouseButtonPressed(InputAction.CallbackContext context)
		//{
		//	var pressed = context.ReadValue<float>();
		//	initialMousePos = Mouse.current.position.ReadValue();
		//	freeRightMouseButtonPressed = pressed;
		//}
		#endregion
	}

	public enum CameraMovementType { LockedTarget, FreeMove }
}